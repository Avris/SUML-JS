# SUML for JavaScript

![SUML logo](logo.png)

This is a JavaScript implementation of a parser and a dumper of SUML: Simple & Unambiguous Markup Language.
You can check out its specification at [gitlab.com/Avris/SUML](https://gitlab.com/Avris/SUML)

## Installation

    yarn add suml

## Usage

    import Suml from 'suml';
    
    const suml = new Suml();
    
    const parsed = suml.parse("'input'");
    const dump = suml.dump({foo: 'bar', lorem: true})

## Development

    make install
    make test

## Webpack loader

See: [gitlab.com/Avris/SUML-Loader](https://gitlab.com/Avris/SUML-Loader)    
