const parser = new (require('./parser'));
const dumper = new (require('./dumper'));

module.exports = class Suml {
    parse(value) {
        return parser.parse(value);
    }

    dump(value) {
        return dumper.dump(value);
    }
};
