const parseError = (message = '', line = -1, context = '') => {
    const err = new Error(message);
    err.name = 'ParseError';
    err.msg = message;
    err.line = line;
    err.context = context;
    err.apply = function (line) {
        const parts = ['Cannot parse'];
        this.line = line;
        parts.push('line ' + (this.line + 1));
        if (this.context !== undefined) {
            parts.push('near `' + this.context + '`');
        }
        if (this.msg) {
            parts.push(': ' + this.msg);
        }

        this.message = parts.join(' ');

        return this;
    };
    err.apply(line);

    return err;
};

module.exports = class Parser {
    static get INDENT() { return 4; }
    static get INDENT_REGEX() { return new RegExp('^( *)[^ ].*$'); }

    static get CONTEXT_NONE() { return 0; }
    static get CONTEXT_TEXT_VERBATIM() { return 1; }
    static get CONTEXT_TEXT_FOLDED() { return 2; }

    static get CONTEXTS() { return {
        '': Parser.CONTEXT_NONE,
        '|': Parser.CONTEXT_TEXT_VERBATIM,
        '>': Parser.CONTEXT_TEXT_FOLDED,
    }; }

    static get REGEX_EMPTY() { return new RegExp('^ *(#.*)?$'); }

    static get REGEX_PART_COMMENT() { return '(?: *#.*?)?$'; }
    static get REGEX_PART_DATE() { return '(?:\\d\\d\\d\\d-\\d\\d-\\d\\d)'; }
    static get REGEX_PART_TIME() { return '(?:\\d\\d:\\d\\d:\\d\\d(?:[+-]\\d\\d\\d\\d)?)'; }
    static get REGEX_PART_DICT_KEY() { return '([^:#\' {}]+):'; }
    static get REGEX_PART_INLINE_ELEMENT() { return '((?:[^,\']+)|(?:\'[^\']*\'))\\s*,?'; }

    static get REGEX_NULL() { return '~'; }
    static get REGEX_INT_DEC() { return new RegExp('^([+-]?[0-9]+)' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_INT_BIN() { return new RegExp('^([+-])?0b([0-1]+)' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_INT_OCT() { return new RegExp('^([+-])?0o([0-7]+)' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_INT_HEX() { return new RegExp('^([+-])?0x([0-9A-Ea-e]+)' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_INF() { return new RegExp('^([+-])?inf' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_FLOAT() { return new RegExp('^([+-]?[0-9]*\\.[0-9]*([Ee][+-][0-9]+)?)' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_BOOL() { return new RegExp('^(true|false)?' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_DATETIME() { return new RegExp('^(' + Parser.REGEX_PART_DATE + '|' + Parser.REGEX_PART_TIME + '|' + Parser.REGEX_PART_DATE + ' ' + Parser.REGEX_PART_TIME + '|(?:@\\d+))' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_STRING_INLINE() { return new RegExp('^\'((?:[^\']|\'\')*)\'' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_STRING_BLOCK() { return new RegExp('^([>|])' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_DICT() { return new RegExp('^' + Parser.REGEX_PART_DICT_KEY + '( .*?|)$'); }
    static get REGEX_LIST_INLINE() { return new RegExp('^\\[(.*)\\]' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_DICT_INLINE() { return new RegExp('^\\{(.*)\\}' + Parser.REGEX_PART_COMMENT); }
    static get REGEX_LIST() { return new RegExp('^-( .*?|)$'); }

    parse(value) {
        value = value.replaceAll('\r\n', '\n').replaceAll('\r', '\n');

        return this._parseByContext(value.split('\n'), Parser.CONTEXT_NONE);
    }

    _parseByContext(lines, context) {
        if (context === Parser.CONTEXT_TEXT_VERBATIM) {
            return lines.join('\n');
        }

        if (context === Parser.CONTEXT_TEXT_FOLDED) {
            return lines.filter(l => !!l).join(' ');
        }

        return this._doParse(lines);
    }

    _doParse(lines) {
        const lineCount = lines.length;
        let currentLineNumber = {v: -1};
        let data = {};
        let m;

        while (true) {
            currentLineNumber.v++;
            if (currentLineNumber.v >= lineCount) {
                return data;
            }
            const currentLine = lines[currentLineNumber.v];

            if (m = currentLine.match(Parser.REGEX_EMPTY)) {
                continue;
            }

            try {
                if (m = currentLine.match(Parser.REGEX_DICT)) {
                    if (Array.isArray(data)) {
                        if (data.length) {
                            throw parseError('Dict in a list context', currentLineNumber.v, currentLine);
                        }
                        data = {};
                    }
                    data[m[1].trim()] = this._parseBlock(m[2].trim(), lines, currentLineNumber);
                    continue;
                }

                if (m = currentLine.match(Parser.REGEX_LIST)) {
                    if (!Array.isArray(data)) {
                        if (Object.keys(data).length) {
                            throw parseError('List in a dict context', currentLineNumber.v, currentLine);
                        }
                        data = [];
                    }
                    data.push(this._parseBlock(m[1].trim(), lines, currentLineNumber));
                    continue;
                }

                if (!Array.isArray(data) && typeof(data) !== 'object') {
                    throw parseError('', currentLineNumber.v, currentLine);
                }

                if (Array.isArray(data) && data.length) {
                    throw parseError('Scalar in a list context', currentLineNumber.v, currentLine);
                }
                if (typeof(data) === 'object' && Object.keys(data).length) {
                    throw parseError('Scalar in a dict context', currentLineNumber.v, currentLine);
                }

                data = this._parseScalar(lines, currentLine, currentLineNumber);
            } catch (err) {
                if (err.name === 'ParseError') {
                    throw err.apply(currentLineNumber.v);
                }
                throw err;
            }
        }
    }

    // side effects possible: currentLineNumber
    _parseScalar(lines, currentLine, currentLineNumber) {
        let m;

        if (currentLine === Parser.REGEX_NULL) {
            return null;
        } else if (m = currentLine.match(Parser.REGEX_INT_DEC)) {
            return parseInt(m[1]);
        } else if (m = currentLine.match(Parser.REGEX_INT_BIN)) {
            return (m[1] === '-' ? -1 : 1) * parseInt(m[2], 2);
        } else if (m = currentLine.match(Parser.REGEX_INT_OCT)) {
            return (m[1] === '-' ? -1 : 1) * parseInt(m[2], 8);
        } else if (m = currentLine.match(Parser.REGEX_INT_HEX)) {
            return (m[1] === '-' ? -1 : 1) * parseInt(m[2], 16);
        } else if (m = currentLine.match(Parser.REGEX_INF)) {
            return (m[1] === '-' ? -1 : 1) * Infinity;
        } else if (m = currentLine.match(Parser.REGEX_FLOAT)) {
            return parseFloat(m[1]);
        } else if (m = currentLine.match(Parser.REGEX_BOOL)) {
            return m[1] === 'true';
        } else if (m = currentLine.match(Parser.REGEX_DATETIME)) {
            let d = m[1];
            if (d.startsWith('@')) {
                return new Date(parseInt(d.substr(1)) * 1000);
            }
            if (d[2] === ':') {
                d = new Date().toDateString() + ' ' + d;
            }

            return new Date(d);
        } else if (m = currentLine.match(Parser.REGEX_STRING_INLINE)) {
            return m[1].replaceAll("''", "'");
        } else if (m = currentLine.match(Parser.REGEX_STRING_BLOCK)) {
            return this._parseBlock(m[1], lines, currentLineNumber);
        } else if (m = currentLine.match(Parser.REGEX_LIST_INLINE)) {
            const result = [];
            let str = m[1].trim();
            let n;
            while (str.length > 0) {
                if (n = str.match(new RegExp('^' + Parser.REGEX_PART_INLINE_ELEMENT))) {
                    result.push(this._doParse([n[1]]));
                    str = str.substr(n[0].length).trim();
                } else {
                    throw parseError('', currentLineNumber.v, str);
                }
            }
            return result;
        } else if (m = currentLine.match(Parser.REGEX_DICT_INLINE)) {
            const result = {};
            let str = m[1].trim();
            let n;
            while (str.length > 0) {
                if (n = str.match(new RegExp('^' + Parser.REGEX_PART_DICT_KEY  + ' *' + Parser.REGEX_PART_INLINE_ELEMENT))) {
                    result[n[1]] = this._doParse([n[2]]);
                    str = str.substr(n[0].length).trim();
                } else {
                    throw parseError('', currentLineNumber.v, str);
                }
            }
            return result;
        }

        throw parseError('', currentLineNumber.v, currentLine);
    }

    // side effects possible: currentLineNumber
    _parseBlock(value, lines, currentLineNumber) {
        let contextMark = value.trim().substr(0, 1);
        if (contextMark === '#') {
            contextMark = '';
        }

        const context = Parser.CONTEXTS[contextMark] || Parser.CONTEXT_NONE;
        const lineCount = lines.length;

        if (Parser.CONTEXTS[contextMark] === undefined) {
            return this._parseByContext([value], context);
        }

        const newLines = [];
        while (true) {
            currentLineNumber.v++;
            if (currentLineNumber.v >= lineCount) {
                currentLineNumber.v--;
                break;
            }
            const currentLine = lines[currentLineNumber.v];

            if (currentLine.trim() === '') {
                newLines.push('');
            } else {
                const im = currentLine.match(Parser.INDENT_REGEX);
                const blockIndent = Math.floor((im ? (im[1] || '').length : 0) / Parser.INDENT);
                if (blockIndent > 0) {
                    newLines.push(currentLine.substr(Parser.INDENT));
                } else {
                    currentLineNumber.v--;
                    break;
                }
            }
        }

        return this._parseByContext(newLines, context);
    }
};
