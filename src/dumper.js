const each = (obj, callable) => {
    const ret = [];
    let i = 0;
    const len = obj.length === undefined ? Object.keys(obj).length : obj.length;

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            ret.push(
                callable(
                    obj[key],
                    key,
                    i,
                    i + 1 === len
                )
            );
            i++;
        }
    }

    return ret;
};

module.exports = class Dumper {
    static get INDENT() { return '    '; }
    static get MAX_COLUMNS() { return 120; }

    dump(value) {
        return this._doDump(value, 0) + '\n';
    }

    _doDump(value, indent) {
        if (value === null || value === undefined) {
            return (indent ? ' ' : '') + '~';
        } else if (value === true) {
            return (indent ? ' ' : '') + 'true';
        } else if (value === false) {
            return (indent ? ' ' : '') + 'false';
        } else if (value === Infinity) {
            return (indent ? ' ' : '') + 'inf';
        } else if (value === -Infinity) {
            return (indent ? ' ' : '') + '-inf';
        } else if (typeof(value) === 'number') {
            return (indent ? ' ' : '') + value.toString().toLowerCase();
        } else if (value instanceof Date) {
            return (indent ? ' ' : '') + this._formatDateTime(value);
        } else if (typeof(value) === 'string') {
            return (indent ? ' ' : '') + this._dumpString(value, indent);
        }

        if (Array.isArray(value)) {
            return this._dumpList(value, indent);
        }

        return this._dumpDict(value, indent);
    }

    _formatDateTime(dt) {
        const y = dt.getFullYear().toString().padStart(4, '0');
        const m = (dt.getMonth() + 1).toString().padStart(2, '0');
        const d = dt.getDate().toString().padStart(2, '0');

        const h = dt.getHours().toString().padStart(2, '0');
        const i = dt.getMinutes().toString().padStart(2, '0');
        const s = dt.getSeconds().toString().padStart(2, '0');

        const o = -dt.getTimezoneOffset();
        const tz = `${o >= 0 ? '+' : '-'}${Math.floor(Math.abs(o) / 60).toString().padStart(2, '0')}${(Math.abs(o) % 60).toString().padStart(2, '0')}`;

        return `${y}-${m}-${d} ${h}:${i}:${s}${tz}`;
    }

    _dumpString(string, indent) {
        string = string.replaceAll('\r\n', '\n').replaceAll('\r', '\n').replace(/\n+$/g, '');

        if (string.includes('\n')) {
            return this._dumpStringVerbatim(string, indent);
        }

        const maxWidth = Dumper.MAX_COLUMNS - indent * Dumper.INDENT.length;
        if (string.length > maxWidth) {
            return this._dumpStringFolded(string, indent, maxWidth);
        }

        return this._quoteString(string);
    }

    _dumpStringVerbatim(string, indent) {
        let buffer = '|\n';

        each(string.split('\n'), (line, i, _, isLast) => {
            if (line) {
                buffer += Dumper.INDENT.repeat(Math.max(indent, 1)) + line;
            }
            if (!isLast) {
                buffer += '\n';
            }
        });

        return buffer;
    }

    _dumpStringFolded(string, indent, maxWidth) {
        let buffer = '>\n' + Dumper.INDENT.repeat(indent);
        let line = '';
        let keepTrailingSpaces = false;

        each(string.split(' '), (word) => {
            if (word === '') {
                buffer += ' ';
                line += ' ';
                keepTrailingSpaces = true;
                return;
            }

            if (keepTrailingSpaces) {
                buffer += word;
                keepTrailingSpaces = false;
                return;
            }

            if (line.length + word.length > maxWidth) {
                buffer += '\n' + Dumper.INDENT.repeat(Math.max(indent, 1));
                line = '';
            }

            buffer += (line ? ' ' : '') + word;
            line += (line ? ' ' : '') + word;
        });

        return buffer;
    }

    _quoteString(string) {
        return "'" + string.replaceAll("'", "''") + "'";
    }

    _dumpList(list, indent) {
        if (list.length === 0) {
            return (indent ? ' ' : '') + '[]';
        }

        let buffer = '';
        if (indent) {
            buffer += '\n';
        }

        each(list, (v, i, _, isLast) => {
            buffer += Dumper.INDENT.repeat(indent) + '-' + this._doDump(v, indent + 1);
            if (!isLast) {
                buffer += '\n';
            }
        });

        return buffer;
    }

    _dumpDict(dict, indent) {
        if (Object.keys(dict).length === 0) {
            return (indent ? ' ' : '') + '{}';
        }

        let buffer = '';
        if (indent) {
            buffer += '\n';
        }

        each(dict, (v, k, i, isLast) => {
            buffer += Dumper.INDENT.repeat(indent) + k + ':' + this._doDump(v, indent + 1);
            if (!isLast) {
                buffer += '\n';
            }
        });

        return buffer;
    }
};
