const test = require('baretest')('SUML tests');
const assert = require('assert');
const suml = new (require('../src/suml'));
const fs = require('fs').promises;
const path = require('path');

process.env.TZ = 'UTC+2';

for (let testcase of ['empty', 'scalar', 'block', 'indent', 'inline', 'full']) {
    test('dump ' + testcase, async function() {
        const input = (await import(`./data/${testcase}.mjs`)).default;
        const expected = (await fs.readFile(path.resolve(`tests/data/${testcase}-dump.suml`))).toString();

        assert.equal(suml.dump(input), expected)
    });

    test('parse ' + testcase, async function() {
        const input = (await fs.readFile(path.resolve(`tests/data/${testcase}-input.suml`))).toString();
        const expected = (await import(`./data/${testcase}.mjs`)).default;

        assert.deepEqual(suml.parse(input), expected)
    });
}

for (let testcase of ['inlineDictInvalid', 'inlineListInvalid', 'scalarInDict', 'scalarInList', 'twoScalars', 'unquoted']) {
    test('wrong parse ' + testcase, async function() {
        const input = (await fs.readFile(path.resolve(`tests/data-wrong/${testcase}.suml`))).toString();
        const expectedError = input.split('\n')[0].substr(2);

        assert.throws(
            _ => suml.parse(input),
            {
                name: 'ParseError',
                message: expectedError,
            }
        );
    });
}

//suml.parse("# Cannot parse line 2 near `,`\nfoo: {,}\n");

test.run();
