export default {
    'null': null,
    numbers: {
        integer: 123,
        decimal: -12.5,
        exp: 0.14e-8,
        bin: 77, //0b1001101,
        oct: 493, //0o755,
        hex: -12854624, //-0xc42560,
        inf: Infinity,
        '-inf': -Infinity,
        zero: 0,
    },
    strings: {
        foobar: 'lorem ipsum dolor sit amet 😈 unicode',
        quoted: 'foo "bar" b:az, it doesn\'t nor ain\'t #hashtag',
        block: `foo: 'bar'
  indented #OK

test: <strong>ok</strong>`,
        'folded-block': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id metus tellus. Quisque dignissim quam id #ullamcorper rhoncus. Cras ut orci viverra, laoreet purus ut, dictum turpis. Curabitur et porttitor mi. Nulla     condimentum ex eget dignissim commodo. In hac habitasse platea dictumst. Mauris a porta diam. Duis mollis neque odio, ut malesuada leo vestibulum vel. Ut luctus faucibus libero vitae malesuada. Suspendisse nunc lectus, euismod eu tempus bibendum, finibus eu purus.',
        'not-null': '~',
        'not-integer': '123',
        'not-decimal': '-12.5',
        'not-exp': '0.14e-8',
        'not-bin': '0b1001110',
        'not-oct': '0o755',
        'not-hex': '-0xc42560',
        'not-inf': '-Inf',
        'not-zero': '0',
        'not-bool': 'true',
        'not-date': '2019-03-12',
        nested: {
            foo: 'bar',
        },
    },
    list: ['element', 8, null, {}, true],
    bool: {
        'true': true,
        'false': false,
    },
    datetime: {
        datetime: new Date('2019-03-12 19:58:00+0300'),
        timestamp: new Date(1552413480000),
    },
};
